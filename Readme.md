![shields default svg](https://img.shields.io/badge/style-flat-green.svg?longCache=true&style=flat)

![shields custom svg](https://img.shields.io/badge/concept-none-red.svg)

![wikimedia svg](https://upload.wikimedia.org/wikipedia/commons/3/37/Generic_Camera_Icon.svg)

![shields default png](https://img.shields.io/badge/style-flat-green.png?longCache=true&style=flat)

![shields custom png](https://img.shields.io/badge/concept-none-red.png)

![wikimedia png](https://upload.wikimedia.org/wikipedia/commons/thumb/3/37/Generic_Camera_Icon.svg/334px-Generic_Camera_Icon.svg.png)